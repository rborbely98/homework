﻿using System;
using System.IO;
using System.Xml.Linq;

namespace HomeworkBorbely
{
    class XmlToCamelJson
    {

        static void Main()
        {

            var sourceFileName = Path.Combine(Environment.CurrentDirectory, "..\\..\\..\\Source Files\\Document1.xml");
            var targetFileName = Path.Combine(Environment.CurrentDirectory, "..\\..\\..\\Target Files\\Document1.json");

            string input; // put the definition outside so we can access it later

            try
            {
                FileStream sourceStream = File.Open(sourceFileName, FileMode.Open);
                var reader = new StreamReader(sourceStream);
                input = reader.ReadToEnd();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            var xdoc = XDocument.Parse(input);

            SharedMethods sharedMethods = new SharedMethods();

            sharedMethods.SaveXmlToCamelJsonFile(xdoc, targetFileName);
        }
    }
}