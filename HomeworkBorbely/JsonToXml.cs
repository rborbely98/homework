﻿using System;
using System.IO;
using Newtonsoft.Json; // install nuget

namespace HomeworkBorbely
{
    class JsonToXml
    {
        static void Main()
        {
            var sourceFileName = Path.Combine(Environment.CurrentDirectory, "..\\..\\..\\Source Files\\Document2.json");
            var targetFileName = Path.Combine(Environment.CurrentDirectory, "..\\..\\..\\Target Files\\Document2.xml");

            string input; // put the definition outside so we can access it later

            try
            {
                FileStream sourceStream = File.Open(sourceFileName, FileMode.Open);
                var reader = new StreamReader(sourceStream);
                input = reader.ReadToEnd();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            var deserializedDoc = JsonConvert.DeserializeXmlNode(input, "root");

            deserializedDoc.Save(targetFileName);
        }
    }
}