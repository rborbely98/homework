﻿using System;
using System.IO;
using System.Net;
using Newtonsoft.Json; // install nuget

namespace HomeworkBorbely
{
    class JsonToXmlHttp
    {
        static void Main()
        {
            var targetFileName = Path.Combine(Environment.CurrentDirectory, "..\\..\\..\\Target Files\\Document2.xml");

            string URLString = "http://localhost/jsonfile.json";

            string json;

            using (WebClient wc = new WebClient())
            {
                json = wc.DownloadString(URLString);
            }

            var deserializedDoc = JsonConvert.DeserializeXmlNode(json, "root");

            deserializedDoc.Save(targetFileName);
        }
    }
}