﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Linq;

namespace HomeworkBorbely
{
    class XmlToJsonHttp
    {
        static void Main()
        {
            var targetFileName = Path.Combine(Environment.CurrentDirectory, "..\\..\\..\\Target Files\\Document1.json");

            string URLString = "http://localhost/file.xml";

            XmlTextReader XMLreader = new XmlTextReader(URLString);

            var xmlxdoc = XDocument.Load(XMLreader);

            SharedMethods sharedMethods = new SharedMethods();

            sharedMethods.SaveXmlToJsonFile(xmlxdoc, targetFileName);
        }
    }
}