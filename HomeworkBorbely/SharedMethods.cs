﻿using System.IO;
using System.Xml.Linq;
using Newtonsoft.Json; // install nuget
using Newtonsoft.Json.Serialization;
using Formatting = Newtonsoft.Json.Formatting;

namespace HomeworkBorbely
{
    public class SharedMethods
    {
        public void SaveXmlToJsonFile(XDocument xdoc, string targetFile)
        {
            var doc = new Document
            {
                Title = xdoc.Root.Element("title").Value,
                Text = xdoc.Root.Element("text").Value
            };

            var serializedDoc = JsonConvert.SerializeObject(doc);

            FileStream targetStream = File.Open(targetFile, FileMode.Create, FileAccess.Write);

            // StreamWriter is IDisposable
            using (StreamWriter sw = new StreamWriter(targetStream))
            {
                sw.Write(serializedDoc);
            }
        }

        public void SaveXmlToCamelJsonFile(XDocument xdoc, string targetFile)
        {
            var doc = new Document
            {
                Title = xdoc.Root.Element("title").Value,
                Text = xdoc.Root.Element("text").Value
            };

            DefaultContractResolver contractResolver = new DefaultContractResolver
            {
                NamingStrategy = new CamelCaseNamingStrategy()
            };

            var serializedDoc = JsonConvert.SerializeObject(doc, new JsonSerializerSettings
            {
                ContractResolver = contractResolver,
                Formatting = Formatting.Indented
            });


            FileStream targetStream = File.Open(targetFile, FileMode.Create, FileAccess.Write);

            // StreamWriter is IDisposable
            using (StreamWriter sw = new StreamWriter(targetStream))
            {
                sw.Write(serializedDoc);
            }
        }
    }
}
