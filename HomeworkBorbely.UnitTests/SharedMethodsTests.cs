using System;
using Xunit;
using System.IO;
using System.Xml.Linq;

namespace HomeworkBorbely.UnitTests
{
    public class SharedMethodsTests
    {
        SharedMethods sharedMethods = new SharedMethods();

        [Fact]
        public void SaveXmlToJsonFile_InvalidDocumentStructure_Exception()
        {
            var sourceFileName = Path.Combine(Environment.CurrentDirectory, "..\\..\\..\\Source Files\\TestDocument1.xml");
            var targetFileName = Path.Combine(Environment.CurrentDirectory, "..\\..\\..\\Target Files\\TestDocument1.json");
            string input;

            try
            {
                FileStream sourceStream = File.Open(sourceFileName, FileMode.Open);
                var reader = new StreamReader(sourceStream);
                input = reader.ReadToEnd();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            var xdoc = XDocument.Parse(input);

            try
            {
                sharedMethods.SaveXmlToJsonFile(xdoc, targetFileName);
            }
            catch (Exception e)
            {
                Assert.True(e is Exception);
            }

        }
        [Fact]

        public void SaveXmlToJsonFile_ValidDocumentStructure_True()
        {
            var sourceFileName = Path.Combine(Environment.CurrentDirectory, "..\\..\\..\\Source Files\\TestDocument2.xml");
            var targetFileName = Path.Combine(Environment.CurrentDirectory, "..\\..\\..\\Target Files\\TestDocument1.json");
            string input;

            try
            {
                FileStream sourceStream = File.Open(sourceFileName, FileMode.Open);
                var reader = new StreamReader(sourceStream);
                input = reader.ReadToEnd();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            var xdoc = XDocument.Parse(input);

            try
            {
                sharedMethods.SaveXmlToJsonFile(xdoc, targetFileName);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            string json;

            try
            {
                FileStream targetStream = File.Open(targetFileName, FileMode.Open);
                var reader = new StreamReader(targetStream);
                json = reader.ReadToEnd();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            Assert.Contains("Some random text", json);

        }
        [Fact]
        public void SaveXmlToCamelJsonFile_InvalidDocumentStructure_Exception()
        {
            var sourceFileName = Path.Combine(Environment.CurrentDirectory, "..\\..\\..\\Source Files\\TestDocument1.xml");
            var targetFileName = Path.Combine(Environment.CurrentDirectory, "..\\..\\..\\Target Files\\TestDocument1.json");
            string input;

            try
            {
                FileStream sourceStream = File.Open(sourceFileName, FileMode.Open);
                var reader = new StreamReader(sourceStream);
                input = reader.ReadToEnd();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            var xdoc = XDocument.Parse(input);

            try
            {
                sharedMethods.SaveXmlToCamelJsonFile(xdoc, targetFileName);
            }
            catch (Exception e)
            {
                Assert.True(e is Exception);
            }
        }
        [Fact]
        public void SaveXmlToCamelJsonFile_ValidDocumentStructure_True()
        {
            var sourceFileName = Path.Combine(Environment.CurrentDirectory, "..\\..\\..\\Source Files\\TestDocument2.xml");
            var targetFileName = Path.Combine(Environment.CurrentDirectory, "..\\..\\..\\Target Files\\TestDocument1.json");
            string input;

            try
            {
                FileStream sourceStream = File.Open(sourceFileName, FileMode.Open);
                var reader = new StreamReader(sourceStream);
                input = reader.ReadToEnd();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            var xdoc = XDocument.Parse(input);

            try
            {
                sharedMethods.SaveXmlToCamelJsonFile(xdoc, targetFileName);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            string json;

            try
            {
                FileStream targetStream = File.Open(targetFileName, FileMode.Open);
                var reader = new StreamReader(targetStream);
                json = reader.ReadToEnd();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            Assert.Contains("Some random text", json);

        }
    }
}
